﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeMiniPlayerTwo
{
    
   public class YouTubeSearcher
    {
        public string currentMedia { get; set; }
         
        public YouTubeSearcher()
        {

        }

        /// <summary>
        /// Call this method to search YouTube
        /// </summary>
        /// <param name="searchString"> The search term you want.</param>
        /// <returns>A youtube link in the form of string.<returns>
        public async Task<string> Search(string searchString)
        {
            //TODO: Read ApiKey externally vai Json for example
            var youTubeService = new YouTubeService(new BaseClientService.Initializer
            {
                ApiKey = "AIzaSyDBXc0zH0v-mLbOIg1YnSWb0G4sk3H11cs", // Secret key, remove from the application and put is somewhere else!
                ApplicationName = "YouTubeMiniPlayerTwo"
            });

            var searchListRequest = youTubeService.Search.List("snippet");
            searchListRequest.Q = searchString; // replace with your own search term.
            searchListRequest.MaxResults = 1;

            // Call the search.list method to retrieve results matching the specified query term.
            var searchListResponse = await searchListRequest.ExecuteAsync();
            currentMedia = searchListResponse.Items[0].Snippet.Title;
            return "https://www.youtube.com/v/" + searchListResponse.Items[0].Id.VideoId;
        }


    }
}
