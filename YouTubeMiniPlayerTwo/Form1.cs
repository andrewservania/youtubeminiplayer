﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.AxHost;
using static YouTubeMiniPlayerTwo.PlayerStates;

namespace YouTubeMiniPlayerTwo
{
    public partial class Form1 : Form
    {
        private string currentMedia = string.Empty;
        private PlayerStates currentPlayerState;
        private YouTubeSearcher youtubeSearcher;
        private const string AUTOPLAY_ON = "?autoplay=1";

        /// <summary>
        /// Start YouTube Mini Player
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            PositionWindowOnScreen();
           currentPlayerState = new PlayerStates(); // feedback needed: Is this necessary?
           currentPlayerState.CurrentState = MediaPlayerStates.STANDING_BY;
            youtubeSearcher = new YouTubeSearcher();
        }

        /// <summary>
        /// Put the player at the lower center of the screen
        /// </summary>
        private void PositionWindowOnScreen()
        {
            CenterToScreen();
            this.Top += 450;
  
        }

        /// <summary>
        /// Method is called automatically if a key is pressed.
        /// Another method is called if the 'Enter' key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) OnEnter(sender, e);
        }

        /// <summary>
        /// Do a search based on what the user has typed in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnEnter(object sender, KeyEventArgs e)
        {
            
            currentPlayerState.CurrentState = MediaPlayerStates.SEARCHING;
            
            string media = await youtubeSearcher.Search(textBox1.Text);

            currentMedia = media;
            axShockwaveFlash1.Movie = currentMedia + AUTOPLAY_ON;
            mediaFileLabel.Text = "Playing: " + youtubeSearcher.currentMedia;
            button1.Text = "STOP";
            currentPlayerState.CurrentState = MediaPlayerStates.PLAYING;
        }

        /// <summary>
        /// Clear the search bar when the user clicks in the search bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onClick(object sender, EventArgs e)
        {
            if(textBox1.Text.Equals("Search YouTube song here...")) textBox1.Clear();
        }
        
        /// <summary>
        /// Call this method to stop current Youtube video from playing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopPlayButtonClick(object sender, EventArgs e)
        {
            if(currentMedia!=null)
            {
                switch(currentPlayerState.CurrentState)
                {
                    case MediaPlayerStates.STOPPED:
                        axShockwaveFlash1.Movie = currentMedia + AUTOPLAY_ON;
                        button1.Text = "STOP";
                        currentPlayerState.CurrentState = MediaPlayerStates.PLAYING;

                        break;

                    case MediaPlayerStates.PLAYING:
                        axShockwaveFlash1.Movie = currentMedia;
                        button1.Text = "PLAY";
                        currentPlayerState.CurrentState = MediaPlayerStates.STOPPED;

                   break;
                }

            }

        }




    }
}
