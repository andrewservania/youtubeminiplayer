﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YouTubeMiniPlayerTwo
{

    public class PlayerStates
    {
        private string MUSIC_PLAYING_IMAGE = "YouTubeMiniPlayerTwo.musicPlaying.gif";
        private string SEARCHING_IMAGE =  "YouTubeMiniPlayerTwo.loading2.gif";
        public enum MediaPlayerStates
        {
            SEARCHING, PLAYING, STOPPED, ERROR, VEVO_ERROR, UNKOWN_ERROR, STANDING_BY
        }

        private MediaPlayerStates currentState;

        public MediaPlayerStates CurrentState {
            get{ return currentState;  }
            set{
                StateChanged(value);
                ; }
        }

        public PlayerStates()
        {
                
        }

        private void StateChanged(MediaPlayerStates playerState)
        {
            currentState = playerState;
            switch(playerState)
            {
                case MediaPlayerStates.SEARCHING:
                    IsSearching();
                    break;
                case MediaPlayerStates.PLAYING:
                    ISPlaying();
                    break;
                case MediaPlayerStates.STOPPED:
                    IsStoped();
                    break;
                case MediaPlayerStates.ERROR:
                    IsError();
                    break;
                case MediaPlayerStates.VEVO_ERROR: break;
                case MediaPlayerStates.UNKOWN_ERROR: break;
                case MediaPlayerStates.STANDING_BY:
                    IsStandingBy();
                    break;



            }

        }

        private void IsStandingBy()
        {
         
        }

        private void IsError()
        {
            throw new NotImplementedException();
        }

        private void IsStoped()
        {
            var form = Form.ActiveForm as Form1;
            if (form != null)
            {
                form.pictureBox1.Visible = false;
            }

        }


        /// <summary>
        /// Making form fields public and accessing them 
        /// http://stackoverflow.com/questions/717074/how-to-access-form-objects-from-another-cs-file-in-c-sharp
        /// </summary>
        private void ISPlaying()
        {
            var form = Form.ActiveForm as Form1;
            if (form != null)
            {
                form.pictureBox1.Visible = true;
                form.pictureBox1.Image = LoadImage(MUSIC_PLAYING_IMAGE);
            }
        }

        private void IsSearching()
        {
            var form = Form.ActiveForm as Form1;
            if(form != null)
            {
                form.pictureBox1.Visible = true;
                form.pictureBox1.Image = LoadImage(SEARCHING_IMAGE);

            }
        }

        /// <summary>
        /// Load image from embedded resource.
        /// </summary>
        /// <param name="filename">The name of the image including the assembly name,
        /// for example: YouTubeMiniPlayer.Image1.gif</param>
        /// <returns>An Image object</returns>
        private Image LoadImage(string filename)
        {
            System.Reflection.Assembly thisEXE;
            thisEXE = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream file = thisEXE.GetManifestResourceStream(filename);
            return Image.FromStream(file);
        }
    }
}
